export class Usuario {
    id: number;
    nombre: string;
    edad: number;
    cargo: string;
    fechaingreso: Date;
    documento: number;
}