import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Mercancia } from 'src/app/Modelo/Mercancia';
import { ServiceService } from 'src/app/Service/service.service';
import { DialogComponent } from 'src/app/dialog/dialog/dialog.component';
import { Usuario } from 'src/app/Modelo/Usuario';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  mercanciaForm = new FormGroup({
    nombreproducto: new FormControl('', Validators.required),
    cantidad: new FormControl('', Validators.required),
    fechaingreso: new FormControl('', Validators.required),
    documentoregistro: new FormControl('', Validators.required)
  })
  maxDate: Date;
  usuarios: Usuario[];
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(): void {
    this.fechas();
    this.getUsuarios();
  }

  crearMercancia(form: Mercancia) {
    this.service.AgregarMercancia(form).subscribe(data => { console.log(data); });
  }

  getUsuarios() {
    this.service.getUsuarios().subscribe(data => {
      this.usuarios = data;
      console.log(data);
    });
  }

  fechas() {
    this.maxDate = new Date();
  }

}
