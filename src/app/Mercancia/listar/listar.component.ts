import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Mercancia } from 'src/app/Modelo/Mercancia';
import { ServiceService } from '../../Service/service.service';
@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  mercanciaForm = new FormGroup({
    nombreproducto: new FormControl('', Validators.required),
    cantidad: new FormControl('', Validators.required),
    fechaingreso: new FormControl('', Validators.required),
    documentoregistro: new FormControl('', Validators.required)
  })
  /*page: number = 0;*/
  filterPost='';
  mercancias: Mercancia[];
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(): void {
    this.getMercancias();

  }

  getMercancias() {
    this.service.getMercancias().subscribe(data => {
      this.mercancias = data;
    });
  }

  editarMercancia(id: number) {
    this.router.navigate(['edit/', id])
  }

  /*nextPage() {
    this.page += 5;
  }

  prevPage() {
    if (this.page > 0) {
      this.page -= 5;
    }
  }*/
}
