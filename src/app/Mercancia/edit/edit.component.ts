import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Mercancia } from 'src/app/Modelo/Mercancia';
import { Usuario } from 'src/app/Modelo/Usuario';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  mercanciaFormEdit = new FormGroup({
    nombreproducto: new FormControl('', Validators.required),
    id: new FormControl('', Validators.required),
    cantidad: new FormControl('', Validators.required),
    fechaingreso: new FormControl('', Validators.required),
    documentoregistro: new FormControl('', Validators.required)
  })
  usuarios: Usuario[];
  mercancia: Mercancia;
  mercancias: Mercancia[];
  maxDate: Date;
  constructor(private activerouter: ActivatedRoute, private router: Router, private service: ServiceService) { }

  ngOnInit(): void {
    let mercanciaId = this.activerouter.snapshot.paramMap.get('id') || "";
    this.getMercancia(mercanciaId);
    this.getUsuarios();
    this.getMercanciaLog(mercanciaId);
  }

  getMercancia(id: string) {
    this.service.getMercancia(id).subscribe(data => {
      this.mercancia = data;
      this.mercanciaFormEdit.setValue(
        {
          'nombreproducto': this.mercancia.nombreproducto,
          'id': this.mercancia.id,
          'cantidad': this.mercancia.cantidad,
          'fechaingreso': this.mercancia.fechaingreso,
          'documentoregistro': this.mercancia.documentoregistro,
        },
      );
      console.log(this.mercanciaFormEdit.value);
    });
  }

  getMercanciaLog(id: string) {
    this.service.getMercanciaLog(id).subscribe(data => {
      this.mercancias = data;
      console.log(data);
    });
  }

  getUsuarios() {
    this.service.getUsuarios().subscribe(data => {
      this.usuarios = data;
      console.log(data);
    });
  }

  editMercancia(form: Mercancia) {
    this.service.modificarMercancia(form).subscribe(data => { console.log(data); });
    return this.router.navigate(['edit/',form.id]);
  }

  eliminarMercancia() {
    let datos:Mercancia=this.mercanciaFormEdit.value;
    this.service.eliminarMercancia(datos).subscribe(data => { console.log(data); });
    return this.router.navigate(['listar']);
  }

  fechas() {
    this.maxDate = new Date();
  }

}
