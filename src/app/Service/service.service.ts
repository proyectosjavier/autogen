import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Mercancia } from '../Modelo/Mercancia';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ResponseI } from '../Modelo/response.interface';
import { Usuario } from '../Modelo/Usuario';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  url: string = "http://localhost:8080/";
  constructor(private http: HttpClient) { }

  getMercancias(): Observable<Mercancia[]> {
    let direccion = this.url + "mercancias";
    return this.http.get<Mercancia[]>(direccion);
  }

  getMercancia(id: string): Observable<Mercancia> {
    let direccion = this.url + "mercancia?id=" + id;
    return this.http.get<Mercancia>(direccion);
  }

  getMercanciaLog(id: string): Observable<Mercancia[]> {
    let direccion = this.url + "mercanciaslog?id=" + id;
    return this.http.get<Mercancia[]>(direccion);
  }

  AgregarMercancia(form: Mercancia): Observable<ResponseI> {
    let direccion = this.url + "addmercancia";
    console.log(direccion);
    return this.http.post<ResponseI>(direccion, form);
  }

  modificarMercancia(form: Mercancia): Observable<ResponseI> {
    let direccion = this.url + "updatemercancia";
    console.log(direccion);
    return this.http.post<ResponseI>(direccion, form);
  }

  eliminarMercancia(form: Mercancia) {
    let direccion = this.url + "mercancia";
    let Options = {
      Headers: new HttpHeaders({
        'Content-type': 'application/json'
      }),
      body: form
    }
    console.log(direccion);
    return this.http.delete<Mercancia>(direccion, Options);
  }

  getUsuarios(): Observable<Usuario[]> {
    let direccion = this.url + "usuario";
    console.log(direccion);
    return this.http.get<Usuario[]>(direccion);
  }

  crearUsuario(form: Usuario): Observable<ResponseI> {
    let direccion = this.url + "usuario";
    console.log(direccion);
    return this.http.post<ResponseI>(direccion, form);
  }

}
