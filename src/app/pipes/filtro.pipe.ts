import { Pipe, PipeTransform } from '@angular/core';
import { Mercancia } from '../Modelo/Mercancia';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPosts = [];
    for (const post of value) {
      if (post.title.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultPosts.push(post);
      }
    }
    return resultPosts;
  }

  /*transformPage(mercancias: Mercancia[], page: number = 0): Mercancia[] {
    return mercancias.slice(page, page + 5);
  }*/

}
