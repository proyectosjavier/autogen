import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/Modelo/Usuario';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  usuarios: Usuario[];
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(): void {
  }

  getUsuarios() {
    this.service.getUsuarios().subscribe(data => {
      this.usuarios = data;
      console.log(data);
    });
  }

}
