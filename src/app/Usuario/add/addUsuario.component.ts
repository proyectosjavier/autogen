import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/Modelo/Usuario';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-add',
  templateUrl: './addUsuario.component.html',
  styleUrls: ['./addUsuario.component.css']
})
export class AddUsuarioComponent implements OnInit {

  usuarioForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    edad: new FormControl('', Validators.required),
    cargo: new FormControl('', Validators.required),
    fechaIngreso: new FormControl('', Validators.required),
    documento: new FormControl('', Validators.required)
  })
  minDate: Date;
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(): void {
    this.fechas();
  }

  crearMercancia(form: Usuario) {
    this.service.crearUsuario(form).subscribe(data => { console.log(data); });
  }

  fechas() {
    this.minDate = new Date();
  }

}
